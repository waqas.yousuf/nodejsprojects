const myExpress = require('express')
const myBodyParser = require('body-parser')

const app = myExpress()

app.use(myBodyParser.urlencoded({extended: false}))
app.use('/login', myExpress.static(__dirname + '/public'))

app.get('/', (req, res) => {
    res.send("Hello Application")
})

app.post('/login', (req, res) => {
    console.log(req.body.email)
    res.redirect('/')
})

app.listen(3000, () => {
    console.log('Server is running ....');
})
