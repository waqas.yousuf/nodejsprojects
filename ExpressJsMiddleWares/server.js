const myExpress = require('express')
const app = myExpress()

const myFirstMiddleWare = (req, res, next) => {
    console.log("I am a middleware")
    next()
}

const mySecondMiddleWare = (req, res, next) => {
    req.requestTime = Date.now()
    next()
}

app.use(myFirstMiddleWare)
app.use(mySecondMiddleWare)

app.get('/', (req, res) => {
    res.send("Hello World!!!! and the request time was " + req.requestTime)
    console.log('Hello from the home directory')
})



app.listen(3000, () => console.log("Server is serving at port 3000..."))