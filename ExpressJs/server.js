const myExpress = require('express')
const app = myExpress()

app.get('/', (req, res) => {
    res.send("Hello World!!!!")
})

app.get('/about', (req, res) => {
    // res.status(200).json({user: "waqas", id: "123ght7656"})
    res.status(500).json('Page not found man!!!')
})

app.get('/contact', (req, res) => {
    res.send("<h1>Hey you can contact me here!</h1>")
})

app.get('/services', (req, res) => {
    res.send(`
        <h1>Services Page</h1>
        <ul>
            <li>Web Development</li>
            <li>Mobile App Development</li>
            <li>Graphics Designing</li>
        </ul>
    `)
})

app.delete("/deletepage", (req, res) => {
    res.send("Hello I am going to delete you Now!")
})

app.get('/ab*cd', (req, res) => {
    res.send("I am the Regex page!!!!")
})

app.get('/user/:id/post/:post_id', (req, res) => {
    res.send(req.params)
})

app.get('/flights/:from-:to', (req, res) => {
    res.send(req.params)
})

app.listen(3000, () => console.log("Server is serving at port 3000..."))