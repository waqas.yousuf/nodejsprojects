const express = require('express')
const passport = require('passport')
const Strategy = require('passport-facebook').Strategy

passport.use(new Strategy(
        {
            clientID: "1088809155344267",
            clientSecret: "ef3474ccb7b293c929fcda4b2851eb9c",
            callbackURL: "http://localhost:3000/login/facebook/return"
        }, (accessToken, RefreshToken, profile, cb) => {
            return cb(null, profile)
        }
    )         
);

passport.serializeUser(function (user, cb) {
    cb(null, user);
})

passport.deserializeUser(function (obj, cb) {
    cb(null, obj)
})

const app = express()

app.set('views', __dirname + "/views")
app.set('view engine', 'ejs')

app.use(require("morgan")("combined"))
app.use(require("cookie-parser")())
app.use(require("body-parser").urlencoded({ extended: true }))
app.use(require("express-session")({
    secret: "lco app",
    resave: true,
    saveUninitialized: true
}))


//@route    - GET  /home
//@desc     - a route to home page
//@access   - PUBLIC
app.get('/', (req, res) => {
    res.render("home.ejs", { user: req.user })
})

//@route    - GET  /login
//@desc     - a route to login page
//@access   - PUBLIC
app.get('/login', (req, res) => {
    res.render('login.ejs')
})

//@route    - GET  /login/facebook
//@desc     - a route to facebook auth
//@access   - PUBLIC
app.get("/login/facebook", passport.authenticate("facebook"))

//@route    - GET  /login/facebook/callback
//@desc     - a route to facebook auth
//@access   - PUBLIC
app.get("login/facebook/callback", passport.authenticate("facebook", {
    failureRedirect: "/login"
}), (req, res) => {
    // in case of successfull authentication redirect to home
    res.redirect('/')
})

//@route    - GET  /profile
//@desc     - a route to profile
//@access   - PRIVATE
app.get("/profile", require("connect-ensure-login").ensureLoggedIn(), (req, res) => {
    res.render("/profile.ejs")
})

app.listen(3000)