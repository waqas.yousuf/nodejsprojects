const express = require('express')
const router = express.Router()
const bcryptjs = require('bcryptjs')
const jsonwt = require('jsonwebtoken')
const passport = require('passport')
const key = require('../../setup/myurl')

router.get('/', (req, res) => {
    res.json({message: 'Auth is being tested'})
})

// importing schema
const Person = require('../../models/Person')
router.post('/register', (req, res) => {
    Person.findOne({
        email: req.body.email
    })
    .then((person) => {
        if(person){
            return res.status(400).json({message: "Email already registered"})
        }else{
            let profilepic = req.body.gender == "male" ? "malepic" : "femalepic"
            const newPerson = new Person({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password,
                profilepic: profilepic,
                gender: req.body.gender
            })

            bcryptjs.genSalt(10, (err, salt) => {
                bcryptjs.hash(newPerson.password, salt, (err, hash) => {
                    if(err) throw err
                    newPerson.password = hash
                    newPerson.save()
                        .then(person => res.json(person))
                        .catch(err => console.log(err))
                })
            })
        }
    })
    .catch(err => console.log(err))
})

router.post("/login", (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    Person.findOne({ email })
        .then((person) => {
            if(!person){
                return res.status(400).json({message: "User not found"});
            }
            bcryptjs.compare(password, person.password)
                .then(authenticated => {
                    if(!authenticated){
                        return res.status(400).json({message: "Incorrect Password"})
                    }else{
                        // return res.status(200).json({message: "User logged in successfully"})
                        // use payload and create token for user
                        const payload = {
                            id: person.id,
                            name: person.name,
                            email: person.email,
                            username: person.username
                        }
                        jsonwt.sign(
                            payload, 
                            key.secret, 
                            {expiresIn: 3600},
                            (err, token) => {
                                if (err) throw err
                                res.json({
                                    success: true,
                                    token: 'Bearer ' + token
                                })
                            }
                        )
                    }
                })
                .catch(err => console.log(err))
        })
        .catch(err => console.log(err));

})

router.get("/profile", passport.authenticate("jwt", {session: false}), (req, res) => {
    res.json({
        id: req.user.id,
        name: req.user.name,
        email: req.user.email,
        profilepic: req.user.profilepic,
        gender: req.user.gender
    })
})




module.exports = router