const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const passport = require('passport')

const Person = require('../../models/Person')
const Profile = require('../../models/Profile')
const Question = require('../../models/Question')

router.get('/', (req, res) => {
    Question.find().sort({ date: "desc" }).then((questions) => {
        res.json(questions)
    }).catch(err => console.log(err))
})

router.post("/", passport.authenticate("jwt", {session: false}), (req, res) => {
    const newQuestion = new Question({
        textone: req.body.textone,
        texttwo: req.body.texttwo,
        name: req.user.name,
        user: req.user.id
    })

    newQuestion.save()
    .then(question => res.json(question))
    .catch(err => console.log(err))

})

router.post("/answers/:id", passport.authenticate("jwt", {session: false}), (req,res) => {
    Question.findById(req.params.id).then(question => {
        const newAnswer = {
            user: req.user.id,
            name: req.body.name,
            text: req.body.text
        }
        question.answers.unshift(newAnswer)
        question.save().then(question => {
            res.json(question)
        }).catch(err => console.log(err))
    }).catch(err => console.log(err))
})

router.post("/upvotes/:id", passport.authenticate("jwt", {session: false}), (req, res) => {
    Profile.findOne({ user: req.user.id }).then(profile => {
        Question.findById(req.params.id).then(question => {
            if(question.upvotes.filter(upvote => upvote.user.toString() === req.user.id.toString()).length > 0)
            {
                return res.status(400).json({"message": "You have already upvoted this question"})
            }
            question.upvotes.unshift({ user: req.user.id })
            question.save().then(question => res.json(question)).catch(err => console.log(err))
        }).catch(err => console.log(err))
    }).catch(err => console.log(err))
})

module.exports = router