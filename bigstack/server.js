const express = require('Express')
const mongoose = require('mongoose')
const bodyparser = require('body-parser')

const app = new express()

// middleware for body parser
app.use(bodyparser.urlencoded({extended: false}))
app.use(bodyparser.json())

const profile = require('./routes/api/profile')
const questions = require('./routes/api/question')

// mongodb config
const db = require('./setup/myurl').mongoURL
const auth = require('./routes/api/auth')
const passport = require('passport')

// databaes connectivity
mongoose
    .connect(db)
    .then(
        () => console.log('connected with Db')
    )
    .catch(err => console.log(err))

// passport middleware
app.use(passport.initialize())

// config for jwt strategy
require('./strategies/jsonwtStrategy')(passport)

// test route
app.get('/', (req, res) => {
res.send("Hey Big Stack")
})

//actual routes
app.use('/api/auth', auth)
app.use('/api/profile', profile)
app.use('/api/questions', questions)
    
const port = process.env.PORT || 3000
app.listen(port)