const http = require("http")
const fs = require("fs")
const url = require("url")
const path = require("path")
const port = 5000
const host = "127.0.0.1"
const mimeTypes = {
    html: "text/html",
    css: "text/css",
    js: "text/js",
    png: "image/png",
    jpg: "image/jpg",
    jpeg: "image/jpeg"
}

http.createServer((req, res) => {
    var myUrl = url.parse(req.url).pathname
    var filename = path.join(process.cwd(), unescape(myUrl))
    console.log("File you are looking for is: " + filename)
    try {
        var loadFile = fs.lstatSync(filename)
    } catch (error) {
        res.writeHead(404, {"Content-Type": 'text/plain'})
        res.write("404 Page not found")
        res.end()
        return
    }

    if(loadFile.isFile()){
        var mimeType = mimeTypes[path.extname(filename).split(".").reverse()[0]]
        res.writeHead(200, {"Content-Type": mimeType})
        var filesStream = fs.createReadStream(filename)
        filesStream.pipe(res)
    }else if(loadFile.isDirectory()){
        res.writeHead(302, {location: "index.html"})
        res.end();
    }else{
        res.writeHead(500, {'Content-Type': 'text/plain'})
        res.write('500 Internal Server Error')
        res.end()
    }
}).listen(port, host, () => {
    console.log(`Server is running at ${host}:${port}`)
})
